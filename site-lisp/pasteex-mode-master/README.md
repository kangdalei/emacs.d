# kdl 说明 2021-03-28
0. 原理：elisp调用pasteex命令行程序，将剪贴板上图像保存成文件。elisp源文件，有修改。
1. 由于pasteex的bug （https://github.com/huiyadanli/PasteEx/issues/25），必须使用PasteEx.v1.1.8.0.beta.zip 版本。
2. 使用方法：首先用截图软件，截屏保存到剪贴板，然后调用快捷键 <spc>pi ，输入链结名称。
3. 图片的保存名称是自动命名。假设markdown文件名：kkk.md, 图片的保存目录是同位置下新建一目录 .\kkk_imgs
4. 如果markdown文件名有汉字，为避免引起错误，图片目录名会截去汉字。所以markdown文件，要以英文开头。


# What's this?
It's an Emacs extension, using it you can just use one key to save clipboard image to disk file, and at the same time insert the file link(org-mode/markdown-mode) or file path(other mode) to current point.

Here is a usage demo:
![](./img/illustrate.gif)

Based on PasteEx, only support Windows.

# Prerequisite
- Install [PasteEx](https://github.com/huiyadanli/PasteEx/releases)

# Installation
Put `pasteex-mode.el` to your `load-path`. The `load-path` is usually `~/elisp/`. It's set in your `~/.emacs` file like this:

```emacs-lisp
(add-to-list 'load-path (expand-file-name "~/elisp"))
(require 'pasteex-mode)
```

# Basic Usage
- Add `PasteEx.exe` executable to environment PATH, or set the variable `pasteex-executable-path` in your config file, like this:

```emacs-lisp
(setq pasteex-executable-path "D:/program/PasteEx/PasteEx.exe")
```

- Bind your favorite key to function `pasteex-image`, like this:

```emacs-lisp
(global-set-key (kbd "C-x p i") 'pasteex-image)
```

- After you make a screenshot to clipboard, or copy a PNG image file to clipboard, then just press `C-x p i` shortcut, and the file link or path will be inserted to your buffer immediately, the screenshot image file is saved to `./img/` directory by default.

# Feature List
Support these functions:
- `pasteex-image`: Save clipboard image to disk file, and insert file path to current point.
- `pasteex-delete-img-link-and-file-at-line`: Delete image link at line, and delete related disk file at the same time.
- `pasteex-is-png-file`: Check a file is png file or not.

# Tips
- Only support Windows, because PasteEx only support Windows now.
- That's all, enjoy it :)
