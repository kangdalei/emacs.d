﻿;;; init.el --- Initialization file
;; Written by kdl  2020

;;---------------------------------------------------------------------
;; 增加启动期间垃圾回收的阈值，启动结束后还原。
;; 设置启动时file-name-handler-alist为nil

(setq gc-cons-threshold most-positive-fixnum
      gc-cons-percentage 0.5)
(defvar  default-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)
(add-hook 'emacs-startup-hook '(lambda ()
                                 (setq file-name-handler-alist default-file-name-handler-alist)
                                 (setq gc-cons-threshold (* 9 1024 1024)) ; 9M
                                 ))
;;--------------------------------------------------------------------
;; 显示启动时间
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;;--------------------------------------------------------------------
;; 把目录lisp, 存放elpa插件配置，添加到搜索路径中去
(add-to-list
    'load-path
    (expand-file-name "lisp" user-emacs-directory))

;; 把site-lisp目录及其子目录，存放非elpa插件，添加到搜索路径
;; (add-to-list
;;     'load-path
;;     (expand-file-name "site-lisp" user-emacs-directory))

(let ((site-lisp-dir (expand-file-name "site-lisp" user-emacs-directory)))
    (add-to-list 'load-path site-lisp-dir) ; site-lisp 路径加入load-path
    (let ((default-directory  (concat site-lisp-dir "/")))
    (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
        (normal-top-level-add-subdirs-to-load-path)))) ;  子目录也都加入load-path

;; 设置 customize 配置的存放文件
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

;;------------------   lisp   -----------------------------
(require 'init-config);个人基础配置, 不需要网络
(require 'init-elpa) ; 配置插件仓库, 安装use-package
(require 'init-packages); 常用插件安装
(require 'init-latex)
(require 'init-org)
(require 'init-hydra)
;(require 'init-xahk) ; ahk Mode  emacs27 有问题 待修
(require 'init-web) ; web js mode
(require 'kdl-functions-autoloads) ; 加载自定义函数

(require 'init-evil); evil 全家桶

(require 'init-keyfreq); 记录命令频率



;;------------------- 读取 customize 文件 -----------------
(when (file-exists-p custom-file)
  (load-file custom-file))
;;-------------------临时宏 -------------------------------
(fset 'mb
   [?\C-c ?\C-s ?b ?F ?* ?a ?\[ ?1 escape ?f ?* ?\;])

(global-set-key (kbd "<f9>") 'mb)


;;;; init.el ends here
