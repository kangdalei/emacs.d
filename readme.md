# emacs配置项目顺利安装运行须知

基于 ~~emacs 26.1, windows7~~    Emacs 27.2  Windows 10 (LTSC 2019)撰写, ~~但是目标是跨平台通用~~ 跨平台没有充分测试过. 本文件记录各平台
差异, 以及第三方依赖. 为我在不同计算机顺利安装配置提供方便.

## 平台差异

init-config是第一个加载文件, 进行基础配置. 只加载这个配置, 基本能用.

在lisp/init-config.el文件里, 通过定义*is-linux* *is-windows* *is-mac*
三个全局常量, 作为不同平台标志位.

Windows平台英文字体设置为
[Hack](https://github.com/source-foundry/Hack-windows-installer/releases), 中
文字体为系统自带 "微软雅黑", 字体大小需根据不同计算机单独设置. 详见 `lisp/init-config.el` 文件.

Org 或 Markdown 文件中英文表格对齐, 使用 [valign](https://github.com/casouri/valign) 插件.

init-evil 文件加载evil系列全家桶. 在不同平台, 对输入法管理不同.
Windows平台使用外部系统输入法. Linux平台终端下, 使用emacs内置的pyim输
入法(代码还未写).

### Windows系统的 HOME 设置 ###

默认位置: [The Emacs Initialization File](https://www.gnu.org/software/emacs/manual/html_node/emacs/Init-File.html)

[HOME and Startup Directories on MS-Windows](https://www.gnu.org/software/emacs/manual/html_node/emacs/Windows-HOME.html)

1. 如果有环境变量HOME, 寻找下面的.emacs
2. 没有环境变量, 寻找注册表中的HOME
3. 如果环境变量和注册表都没有HOME, 把C:\Users\username\AppData\Roaming (Win7及以后) 作为HOME.


如果不确定, 可在在Emacs中执行 ` C-x d ~/ <RET> `, 查看结果.

建议在如下目录:
```
C:\Users\%UserName%\AppData\Roaming\
```
建立 .emacs 文件, 内容如下:

```
;; This file is only for windows 7/8/8.1
;; The only thing it does is to set the HOME directories for emacs,
;; then trigger the init.el in the directory specified by HOME to
;; accomplish the true initialization
;; You should put this file in the **default** HOME directory right after
;; emacs is installed
(setenv "HOME" "C:/Users/kdl/") ;; you can change this dir to the place you like
(load "~/.emacs.d/init.el")
```

注: Windows 资源管理器无法直接建立以 . 开头的文件, 可以使用emacs C-x
C-f 建立, 或者直接把已存在的 .emacs 文件粘贴过来.

在目录` C:\Users\%UserName%\ ` 下建立 .emacs.d 目录

### Windows系统的 右键菜单添加 emacs 编辑 ###

打开注册表编辑器 regedit

```
[HKEY_CLASSES_ROOT\*\shell]

[HKEY_CLASSES_ROOT\*\shell\EmacsEdit]
@="GNU Emacs (&A)"
# The above value appears in the global context menu, 
# i.e., when you right click on a file.
# (The '&' makes the next character a shortcut.)

[HKEY_CLASSES_ROOT\*\shell\EmacsEdit\command]
@="C:\\Users\\kdl\\app\\emacs-27.2-x86_64\\bin\\runemacs.exe \"%1\""
# The above has to point to where you install Emacs
```

路径根据实际情况写。在`dos_bat`目录下，运行`edit_with_emacs2right_click_menu.reg`



## evil-mode 的 leader 键设置 ##
使用 general 的 leader 设置, `<SPC>` 和 `,` 都设为 leader 键.
详情见 lisp/init-evil.el

evil-mode 下的evil-repeat-find-char-reverse 绑定到 `<SPC>;`


## autoload 自定义函数 ##
在 lisp/kdl-functions.el 内, 存放自定义函数. 每次新增或移除函数后, 执行
第四行的 update-file-autoloads , 生成autoload文件.


## 额外的第三方程序需求:

1.  evil自动切换输入法依赖插件
sis([smart-input-source](https://github.com/laishulu/emacs-smart-input-source)),
在不同操作系统平台依赖不同程序.  windows平台依赖 **im-select.exe**.  需要
手动安装, 放入系统PATH目录中.  sis目前(2020.7.23)并不稳定, 从github下
载最新, 放入site-lisp目录.

2. elpa-mirror插件, 备份packages, git clone下载到site-lisp目录. 需要命
   令行程序**tar**. windows平台, 可通过安装cygwin来提供.
3. 如果使用 `counsel-rg` 命令, 需要 ripgrep 支持.
4. `find-file-in-project` 插件, 在 windows  系统需要 `find` 命令. 可安
   装cygwin 来获取find 命令. 注意, windows 系统自身也有一个 find 命令,
   cygwin的路径应添加在环境变量 path 前面. Gow
   (<https://github.com/bmatzelle/gow>)也有类似的命令.
5. windows平台, 截图插入插件 `pasteex-mode`, 需要借助
   [pasteEx.exe](https://github.com/huiyadanli/PasteEx/wiki), 1.1.8.0版.  ~~因为
   从剪贴板图片生成的文件名含有 `buffer-file-name` , 不要用中文, 否则有麻烦.~~
   对源代码进行了修改，删除了文件名中汉字及以后字符串，所以markdown文件名必需以英文开始。如果是Windos平台，且控制台的编码不是utf-8(默认是cp936) markdown文件的存储路径不能有中文。
6. 对于 latex 编写, Windows 平台 PDF 浏览器, 需要安装 SumatraPDF.
7. nodejs-repl 插件需要系统安装 nodejs.
8. wucuo 拼写检查插件 需要系统安装 Aspell or Hunspell and the dictionaries at
   first.  Windows 平台使用 cygwin安装了 Aspell及词典. Hunspell 安装，目前失败。

~~目前需要手动安装unto-tree~~ undo-tree 已不需要。



## 备份插件elpa-mirror 用法 ##

注:本配置已安装 elpa-mirror, 可直接使用  elpamr-create-mirror-for-installed 命令

https://github.com/redguardtoo/elpa-mirror

Install

    Download elpa-mirror.el to somewhere (say ~/.emacs.d/site-lisp/elpa-mirror/)
    Add below code into your ~/.emacs,

(add-to-list 'load-path "~/.emacs.d/site-lisp/elpa-mirror")
(require 'elpa-mirror)


Usage

M-x elpamr-create-mirror-for-installed to create local repository.

To use the repository ~/myelpa/, insert below code into your ~/.emacs,

;; myelpa is the ONLY repository now, dont forget trailing slash in the directory
(setq package-archives '(("myelpa" . "~/myelpa/")))

To update existing local repository, run M-x elpamr-create-mirror-for-installed again.

BTW, you can run elpa-mirror.el as a independent script,

mkdir -p ~/myelpa && emacs --batch -l ~/.emacs.d/init.el -l ~/any-directory-you-prefer/elpa-mirror.el --eval='(setq elpamr-default-output-directory "~/myelpa")' --eval='(elpamr-create-mirror-for-installed)'

Tips
Change output directory,

(setq elpamr-default-output-directory "~/myelpa")

Repository on Dropbox

Insert below code into ~/.emacs:

;; all-to-list will not override default elpa.
;; So now you have two repositories.
;; One is GNU elpa. Another is myelpa
(add-to-list 'package-archives
             '("myelpa" . "https://dl.dropboxusercontent.com/u/858862/myelpa/"))

Repository on GitHub

My repository is https://github.com/redguardtoo/myelpa.

Insert below code into .emacs:

(add-to-list 'package-archives
             '("myelpa" . "https://raw.githubusercontent.com/redguardtoo/myelpa/master/"))

    Insert (setq elpamr-debug t) into .emacs
    Reproduce bug and report at https://github.com/redguardtoo/elpa-mirror
