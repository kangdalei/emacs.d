;;; kdl-functions-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "kdl-functions" "kdl-functions.el" (24469 800
;;;;;;  0 0))
;;; Generated autoloads from kdl-functions.el

(autoload 'kdl-increment-number-decimal "kdl-functions" "\
Increment the number forward from point by 'arg'.

\(fn &optional ARG)" t nil)

(autoload 'kdl-decrement-number-decimal "kdl-functions" "\


\(fn &optional ARG)" t nil)

(autoload 'kdl-increment-number-at-point "kdl-functions" "\
Increment number at point like vim's C-a

\(fn &optional INCREMENT)" t nil)

(autoload 'kdl-decrement-number-at-point "kdl-functions" "\
Decrement number at point like vim's C-x

\(fn &optional INCREMENT)" t nil)

(autoload 'kdl-insert-current-time "kdl-functions" "\


\(fn)" t nil)

(autoload 'kdl-switch-to-next-buffer "kdl-functions" "\


\(fn)" t nil)

(autoload 'kdl-switch-to-prev-buffer "kdl-functions" "\


\(fn)" t nil)

(autoload 'kdl-new-empty-buffer "kdl-functions" "\
Create a new empty buffer.
New buffer will be named “untitled” or “untitled<2>”, “untitled<3>”, etc.

It returns the buffer (for elisp programing).

URL `http://ergoemacs.org/emacs/emacs_new_empty_buffer.html'
Version 2017-11-01

\(fn)" t nil)

(autoload 'kdl-hidden-dos-eol "kdl-functions" "\
Do not show ^M in files containing mixed UNIX and DOS line endings.

\(fn)" t nil)

(autoload 'kdl-remove-dos-eol "kdl-functions" "\
Replace DOS eolns CR LF with Unix eolns CR

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "kdl-functions" '(#("kdl-" 0 4 (fontified nil face font-lock-function-name-face)))))

;;;***

(provide 'kdl-functions-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; kdl-functions-autoloads.el ends here
