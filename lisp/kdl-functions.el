;;;; 存放自定义函数  ; -*- lexical-binding: t -*-
;; https://emacs.stackexchange.com/questions/8023/how-to-use-autoload
;; 在新增或移除函数后, 执行下面语句, 自动生成autoloads 文件
;; (update-file-autoloads "kdl-functions.el" t (expand-file-name "kdl-functions-autoloads.el"))

;;---------------------------------------------------------------------
;;https://www.emacswiki.org/emacs/IncrementNumber

;;;###autoload
(defun kdl-increment-number-decimal (&optional arg)
  "Increment the number forward from point by 'arg'."
  (interactive "p*")
  (save-excursion
    (save-match-data
      (let (inc-by field-width answer)
        (setq inc-by (if arg arg 1))
        (skip-chars-backward "0123456789")
        (when (re-search-forward "[0-9]+" nil t)
          (setq field-width (- (match-end 0) (match-beginning 0)))
          (setq answer (+ (string-to-number (match-string 0) 10) inc-by))
          (when (< answer 0)
            (setq answer (+ (expt 10 field-width) answer)))
          (replace-match (format (concat "%0" (int-to-string field-width) "d")
                                 answer)))))))

;;;###autoload
(defun kdl-decrement-number-decimal (&optional arg)
  (interactive "p*")
  (kdl-increment-number-decimal (if arg (- arg) -1)))



(defun kdl-change-number-like-vim (change increment)
  (save-excursion
    ;; (skip-chars-backward "0123456789")
    ;; (re-search-forward "[0-9]+" nil t)
    ;; (backward-char)
    (let ((number (number-at-point)))
      (unless number
        (re-search-forward "[0-9]+" nil t)
        (backward-char)
        (setq number (number-at-point))
        )
      (when number
        (progn
          (forward-word)
          (search-backward (number-to-string number))
          (replace-match (number-to-string (funcall change number increment)))
          )))))


;;;###autoload
(defun kdl-increment-number-at-point (&optional increment)
  "Increment number at point like vim's C-a"
  (interactive "p")
  (kdl-change-number-like-vim '+ (or increment 1)))

;;;###autoload
(defun kdl-decrement-number-at-point (&optional increment)
  "Decrement number at point like vim's C-x"
  (interactive "p")
  (kdl-change-number-like-vim '- (or increment 1)))

;;---------------------------------------------------------------------

;;---------------------------------------------------------------------
;; 插入当前日期

;;;###autoload
(defun kdl-insert-current-time ()
  (interactive)
  (insert (format-time-string "%Y-%m-%d" (current-time))))

;; (global-set-key (kbd "<f5>") 'kdl-insert-current-time)
;;---------------------------------------------------------------------


;;---------------------------------------------------------------------
;; 切换buffer
;; https://zhuanlan.zhihu.com/ghostinemacs
;; 作者：wolray

;;定义正常buffer
(defun kdl-normal-buffer-p ()
  (or (not buffer-read-only)
      (buffer-file-name)))

;;;###autoload
(defun kdl-switch-to-next-buffer ()
  (interactive)
  (unless (minibufferp)
    (let ((p t) (bn (buffer-name)))
      (switch-to-next-buffer)
      (while (and p (not (kdl-normal-buffer-p)))
    (switch-to-next-buffer)
    (when (string= bn (buffer-name)) (setq p nil))))))

;;;###autoload
(defun kdl-switch-to-prev-buffer ()
  (interactive)
  (unless (minibufferp)
    (let ((p t) (bn (buffer-name)))
      (switch-to-prev-buffer)
      (while (and p (not (kdl-normal-buffer-p)))
    (switch-to-prev-buffer)
    (when (string= bn (buffer-name)) (setq p nil))))))
;;---------------------------------------------------------------------


;;---------------------------------------------------------------------
;; New Empty Buffer
;; http://ergoemacs.org/emacs/emacs_new_empty_buffer.html

;;;###autoload
(defun kdl-new-empty-buffer ()
  "Create a new empty buffer.
New buffer will be named “untitled” or “untitled<2>”, “untitled<3>”, etc.

It returns the buffer (for elisp programing).

URL `http://ergoemacs.org/emacs/emacs_new_empty_buffer.html'
Version 2017-11-01"
  (interactive)
  (let (($buf (generate-new-buffer "untitled")))
    (switch-to-buffer $buf)
    (funcall initial-major-mode)
    (setq buffer-offer-save t)
    $buf
    ))
;;---------------------------------------------------------------------

;;---------------------------------------------------------------------
;; http://book.emacs-china.org/#orgheadline24
;; 处理 dos eol

;;;###autoload
(defun kdl-hidden-dos-eol ()
  "Do not show ^M in files containing mixed UNIX and DOS line endings."
  (interactive)
  (unless buffer-display-table
    (setq buffer-display-table (make-display-table)))
  (aset buffer-display-table ?\^M []))


;;;###autoload
(defun kdl-remove-dos-eol ()
  "Replace DOS eolns CR LF with Unix eolns CR"
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (search-forward "\r" nil t) (replace-match ""))))

;;---------------------------------------------------------------------

;; 宏。临时宏
(fset 'mb
   [?\C-c ?\C-s ?b ?F ?* ?a ?\[ ?1 escape ?f ?* ?\;])















;;---------------------------------------------------------------------
;; Emacs: How to Bind Super Hyper Keys
;; http://ergoemacs.org/emacs/emacs_hyper_super_keys.html
;; make PC keyboard's Win key or other to type Super or Hyper, for emacs running on Windows.
;; (setq w32-pass-lwindow-to-system nil)
;; (setq w32-lwindow-modifier 'super) ; Left Windows key

;; (setq w32-pass-rwindow-to-system nil)
;; (setq w32-rwindow-modifier 'super) ; Right Windows key

;; (setq w32-pass-apps-to-system nil)
;; (setq w32-apps-modifier 'hyper) ; Menu/App key

;; (info "(emacs) Windows Keyboard")

;;Mac OS X

;; set keys for Apple keyboard, for emacs in OS X
;; (setq mac-command-modifier 'meta) ; make cmd key do Meta
;; (setq mac-option-modifier 'super) ; make opt key do Super
;; (setq mac-control-modifier 'control) ; make Control key do Control
;; (setq ns-function-modifier 'hyper)  ; make Fn key do Hyper
