;;;;init-web.el 前端全家桶
;;;; edit the document since 2020-10-24.

;;---------------------------------------------------------------------
;; https://github.com/fxbois/web-mode
(use-package web-mode
  :mode (("\\.htm\\'"  . web-mode)
         ("\\.html\\'" . web-mode)
         )
  :config
  (setq web-mode-markup-indent-offset 2) ; web-mode, html tag in html file
  (setq web-mode-css-indent-offset 2)    ; web-mode, css in html file
  (setq web-mode-code-indent-offset 4)   ; web-mode, js code in html file
  )
;;---------------------------------------------------------------------


;;---------------------------------------------------------------------
;; https://github.com/mooz/js2-mode
(use-package js2-mode
  :mode (("\\.js\\'" . js2-mode)
         )
;; (setq auto-mode-alist
;;       (append
;;        '(("\\.js\\'" . js2-mode))
;;        auto-mode-alist))
  :config
  ;; http://book.emacs-china.org/#orgheadline24
  ;; 增强 imenu 匹配能力
  (defun js2-imenu-make-index ()
        (interactive)
        (save-excursion
          ;; (setq imenu-generic-expression '((nil "describe\\(\"\\(.+\\)\"" 1)))
          (imenu--generic-function '(("describe" "\\s-*describe\\s-*(\\s-*[\"']\\(.+\\)[\"']\\s-*,.*" 1)
                                     ("it" "\\s-*it\\s-*(\\s-*[\"']\\(.+\\)[\"']\\s-*,.*" 1)
                                     ("test" "\\s-*test\\s-*(\\s-*[\"']\\(.+\\)[\"']\\s-*,.*" 1)
                                     ("before" "\\s-*before\\s-*(\\s-*[\"']\\(.+\\)[\"']\\s-*,.*" 1)
                                     ("after" "\\s-*after\\s-*(\\s-*[\"']\\(.+\\)[\"']\\s-*,.*" 1)
                                     ("Function" "function[ \t]+\\([a-zA-Z0-9_$.]+\\)[ \t]*(" 1)
                                     ("Function" "^[ \t]*\\([a-zA-Z0-9_$.]+\\)[ \t]*=[ \t]*function[ \t]*(" 1)
                                     ("Function" "^var[ \t]*\\([a-zA-Z0-9_$.]+\\)[ \t]*=[ \t]*function[ \t]*(" 1)
                                     ("Function" "^[ \t]*\\([a-zA-Z0-9_$.]+\\)[ \t]*()[ \t]*{" 1)
                                     ("Function" "^[ \t]*\\([a-zA-Z0-9_$.]+\\)[ \t]*:[ \t]*function[ \t]*(" 1)
                                     ("Task" "[. \t]task([ \t]*['\"]\\([^'\"]+\\)" 1)))))

  (setq imenu-create-index-function 'js2-imenu-make-index)



  )

;;---------------------------------------------------------------------


;;---------------------------------------------------------------------
;; https://github.com/abicky/nodejs-repl.el
;; Type M-x nodejs-repl to run Node.js REPL.
(use-package nodejs-repl

             )
;;---------------------------------------------------------------------

(provide 'init-web)
