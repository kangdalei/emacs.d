;;;;init-hydra.el 配置不长用, 又难记的命令菜单

;;https://github.com/abo-abo/hydra
(use-package hydra)

;;---------------------------------------------------------------------
;; text zoom
(defhydra hydra-zoom (:color bule
                      :hint nil)
  "
   ^text zoom^
   -----------
_g_: increase
_l_: decrease
"
  ("g" text-scale-increase)
  ("l" text-scale-decrease)
  ("q" nil "cancel")
  )

(global-set-key (kbd "<f6>") 'hydra-zoom/body)
;;---------------------------------------------------------------------
;;---------------------------------------------------------------------
;; window manager
(defhydra hydra-window (:color bule
                      :hint nil)
  "
   ^window size^
   -----------
_h_: enlarge-horizontally
_l_: shrink-horizontally
_j_: enlarge
_k_: shrink
_b_: blance-window
"
  ("h" enlarge-window-horizontally)
  ("l" shrink-window-horizontally)
  ("j" enlarge-window)
  ("k" shrink-window)
  ("b" balance-windows)
  ("q" nil "cancel")
  )

(global-set-key (kbd "C-c C-w") 'hydra-window/body)
;;---------------------------------------------------------------------


(provide 'init-hydra)
