;;;; init-elpa  配置插件仓库, 安装 use-package 插件
(require 'package)

;; 163 镜像
;; (setq package-archives '(("melpa" . "http://mirrors.163.com/elpa/melpa/")
;;                          ("GNU" . "HTTP://MIRRORS.163.COM/ELPA/GNU/")
;;                          ("ORG" . "HTTP://MIRRORS.163.COM/ELPA/ORG/")
;;                          ("EMACSWIKI" . "HTTP://MIRRORS.163.COM/ELPA/EMACSWIKI/")))

;; ELPA 清华大学镜像
;; (SETQ PACKAGE-ARCHIVES '(("MELPA" . "HTTP://MIRRORS.TUNA.TSINGHUA.EDU.CN/ELPA/MELPA/")
;;                          ("gnu" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
;;                          ("org" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/org/")))

;; emacs-china 镜像
 (setq package-archives '(("gnu"   . "http://elpa.emacs-china.org/gnu/")
                          ("melpa" . "http://elpa.emacs-china.org/melpa/")
                          ("org" . "http://elpa.emacs-china.org/org/")))

;; 官方镜像
;; (setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
;;                          ("melpa" . "https://melpa.org/packages/")))

(setq package-check-signature nil) ;国内镜像, 关闭签名验证好一点



;; don't bother with the initialize, although it may cause much startup time,
;; there's no way to avoid this if you use package.el instead of other package
;; manager, like straight.el
(unless (bound-and-true-p package--initialized)
  (package-initialize))

;; these code run only once, when use-package is not installed
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; make use-package default behavior better
;; with `use-package-always-ensure' you won't need ":ensure t" all the time
;; with `use-package-always-defer' you won't need ":defer t" all the time
(setq use-package-always-ensure t
      use-package-always-defer t
      use-package-enable-imenu-support t
      use-package-expand-minimally t)
(require 'use-package)



;; 使用自動更新套件
;; (use-package auto-package-update
;;   :ensure t
;;   :config
;;   (setq auto-package-update-delete-old-versions t)
;;   (setq auto-package-update-hide-results t)
;;   (auto-package-update-maybe))

(provide 'init-elpa)
