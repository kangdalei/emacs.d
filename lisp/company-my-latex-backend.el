;;;; company-my-latex-backend
;; http://sixty-north.com/blog/series/how-to-write-company-mode-backends.html
;; http://sixty-north.com/blog/writing-the-simplest-emacs-company-mode-backend#id9

(require 'company)
(require 'cl-lib)

;; 自定义各种未收录宏包及命令
(defconst my-latex-completions
  '(
    "dsdf"
    "float"
    "geometry"
    "graphicx"
    "nottoc"
    "tocbibind"
    ))

(defun company-my-latex-backend (command &optional arg &rest ignored)
  (interactive (list 'interactive))

  (case command
    (interactive (company-begin-backend 'company-my-latex-backend))
    ;; (prefix (and (eq major-mode 'LaTeX-mode)
    ;;             (company-grab-symbol)))
    (prefix (company-grab-symbol))
    (candidates
    (remove-if-not
      (lambda (c) (string-prefix-p arg c))
      my-latex-completions))))

;; (add-to-list 'company-backends 'company-my-latex-backend)
(provide 'company-my-latex-backend)
