;;;; init-org.el

(with-eval-after-load 'org
    (message "org-mode is loaded.")
    (setq truncate-lines nil) ; 开启视觉上自动折行
    ;; (add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
    (org-babel-do-load-languages 'org-babel-load-languages
                                 '((emacs-lisp . t)
                                   ;; (ruby . t)
                                   (python . t)
                                   ;; (sh . t)
                                   ;; (latex . t)
                                   ;; (C . t)
                                   ;; (R . t)
                                   ))
;;      babel 模式 python语言 scripting mode 模板   输入 <sp  然后tab键
;;      "Structure completion elements.
;; This is a list of abbreviation keys and values.  The value gets inserted
;; if you type `<' followed by the key and then press the completion key,
;; usually `M-TAB'.  %file will be replaced by a file name after prompting
;; for the file using completion.  The cursor will be placed at the position
;; of the `?` in the template.
    (dolist
        (my-org-structure-template
         '(("sp" "#+BEGIN_SRC python :results output\n?\n#+END_SRC")
           ("se" "#+BEGIN_SRC emacs-lisp\n?\n#+END_SRC")))
      (add-to-list 'org-structure-template-alist my-org-structure-template))

;;     (define-key global-map "\C-cl" 'org-store-link)
;;     (define-key global-map "\C-ca" 'org-agenda)
    ;; (setq org-log-done t)
    )


;; (if (require 'toc-org nil t)
;;     (add-hook 'org-mode-hook 'toc-org-enable)
;;   (warn "toc-org not found"))

(provide 'init-org)
