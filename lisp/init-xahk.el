;;;; init-xahk.el
;; http://xahlee.info/mswin/emacs_autohotkey_mode.html
;; http://xahlee.info/mswin/autohotkey.html

(use-package xahk-mode
  :init
  ;; https://www.emacswiki.org/emacs/AutoModeAlist
  ;; Note that \' matches the end of a string, whereas $ matches the
  ;; empty string before a newline. Thus, $ may lead to unexpected
  ;; behavior when dealing with filenames containing newlines. (Should be
  ;; pretty rare… ;))
  (add-to-list 'auto-mode-alist '("\\.ahk\\'" . xahk-mode)) ; \\' 表示匹配缓冲区结尾, 即什么都不加
  ;; ahk 文件强制以 utf-8-with-signature-dos (utf-8-bom)格式存储
  ;; set-buffer-file-coding-system 默认绑定到 C-x <return> f
  :hook (xahk-mode . (lambda () (set-buffer-file-coding-system 'utf-8-with-signature-dos)))
  :config
  ;; https://emacs-china.org/t/company/8975/4
  ;; 支持 %word% 型式变量补全 company补全是以word为单位（而非symbol），这个由当前buffer的major-mode的syntax-table决定
  (modify-syntax-entry ?% "w" xahk-mode-syntax-table)
  ) ; use-package ')' end here

(provide 'init-xahk)
