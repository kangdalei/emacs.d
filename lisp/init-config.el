;;;; init-config  个人基础配置

(setq inhibit-startup-screen t);关掉启动画面
;(menu-bar-mode -1) ; 关闭菜单
(tool-bar-mode -1) ; 关闭工具栏
(scroll-bar-mode -1) ;关闭滚动条
(column-number-mode t) ;状态栏显示行列信息
(setq default-directory "~/"); 设定默认的文件加载路径
(setq frame-title-format '("Emacs@"system-name": %b %+%+ %f"));;在标题栏提示你目前所在位置.


;; 操作系统的判断
(defconst *is-mac* (eq system-type 'darwin))
(defconst *is-linux* (or (eq system-type 'gnu/linux) (eq system-type 'linux)))
(defconst *is-windows* (or (eq system-type 'ms-dos) (eq system-type 'windows-nt)))
(defconst *is-cygwin* (eq system-type 'cygwin))
;(display-graphic-p); 判断是否gui模式

;; 不同系统个性化配置
;;----------------------------------------------------------------------------
;;windows下
;; (when (string-equal system-type "windows-nt")
;;   (when (string-equal system-name "KANGDL") ;kdl用户名下
;;     (setq default-frame-alist
;;           '((top . 50)
;;             (left . 180)
;;             (height . 40)
;;             (width . 110)
;;             ))
;;     (setq default-directory "g:/") ;默认工作目录
;; )
;;   (when (string-equal system-name "PC-201307182222")
;;     (setq default-directory "d:/") ;默认工作目录
;;     (setq initial-frame-alist (quote ((fullscreen . maximized)))) ;启动后自动全屏
;;     )
;;   )

;;字体设置, 解决windows平台因字体导致的卡顿
(when *is-windows*
  (setq inhibit-compacting-font-caches t) ;解决windows平台，中文字体卡顿问题。关闭垃圾回收
  (if (and (string-equal system-name "PC-201307182222") ;thinkpad T420 显示放大了
           (display-graphic-p)) ; GUI界面
      (progn
        ;; 设置英文字体
        ;; https://github.com/source-foundry/Hack-windows-installer/releases
      (set-face-attribute 'default nil :font "Hack 11") ;英文字体
      ;; (set-face-attribute 'default nil :font "DejaVu Sans Mono 11") ;英文字体
      (dolist (charset '(kana han symbol cjk-misc bopomofo)) ;中日韩字体
        (set-fontset-font (frame-parameter nil 'font) charset (font-spec :family "微软雅黑" )))
        ;; (set-fontset-font (frame-parameter nil 'font) charset (font-spec :family "Microsoft YaHei" :size 16)))
      ;; 设置 雅黑 字体相对默认英文字体的缩放系数,  中英文可同时缩放s
      (setq face-font-rescale-alist '(("微软雅黑" . 1.1)))
      ))
  (if (and (string-equal system-name "DESKTOP-KDL") ;thinkpad p50 字体设置
           (display-graphic-p)) ; GUI界面
      (progn
        ;; 设置英文字体
        ;; https://github.com/source-foundry/Hack-windows-installer/releases
        (set-face-attribute 'default nil :font "Hack 11") ;英文字体
        ;; (set-face-attribute 'default nil :font "DejaVu Sans Mono 11") ;英文字体
        (dolist (charset '(kana han symbol cjk-misc bopomofo)) ;中日韩字体
          (set-fontset-font (frame-parameter nil 'font) charset (font-spec :family "微软雅黑" )))
        ;; (set-fontset-font (frame-parameter nil 'font) charset (font-spec :family "Microsoft YaHei" :size 16)))
        ;; 设置 雅黑 字体相对默认英文字体的缩放系数,  中英文可同时缩放s
        (setq face-font-rescale-alist '(("微软雅黑" . 1.1)))
        ))
  )
;; 查看光标下字符详细信息 C-u C-x =  或者 C-u ga
;; 查看字体描述 M-x describe-font 输入 font name

(setq initial-frame-alist (quote ((fullscreen . maximized)))) ;启动后自动全屏

;;编码设置
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq-default buffer-file-coding-system 'utf-8)
(set-buffer-file-coding-system 'utf-8); 当前buffer

;; 以指定编码重新读取文件
;; C-x C-m r (revert-buffer-with-coding-system)
;; 为文件保存时, 设定指定编码
;; C-x RET f (set-buffer-file-coding-system)
;; C-m == RET    C-i == Tab
;;-------------------------------------------------------------------
;; window 管理被转移到init-packages 延迟加载
;; (winner-mode t) ; 窗口布局undo/redo C-c left C-c right
;;分割窗口
;; 改默认垂直分割窗口. 已注释掉。
;;(setq split-height-threshold nil)
;;(setq split-width-threshold 0)

;; 侧边栏行号打开
(when (>= emacs-major-version 26)
  (global-display-line-numbers-mode t))
;;(setq-default display-line-numbers-width 2)
;;(setq-default display-line-numbers-type 'relative) ;相对行号
;;(setq display-line-numbers-current-absolute t)

;;-------------------------------------------------------------------
;; mode-line-format
;; author: redguardtoo 陈斌
;; @see http://emacs-fu.blogspot.com/2011/08/customizing-mode-line.html
;; But I need global-mode-string,
;; @see http://www.delorie.com/gnu/docs/elisp-manual-21/elisp_360.html
;; use setq-default to set it for /all/ modes
;; (setq-default mode-line-format
;;   (list
;;     ;; the buffer name; the file name as a tool tip
;;     '(:eval (propertize "%b " 'face nil 'help-echo (buffer-file-name)))

;;     ;; line and column
;;     "(" ;; '%02' to set to 2 chars at least; prevents flickering
;;     "%02l" "," "%01c"
;;     ") "

;;     ;; @see https://www.gnu.org/software/emacs/manual/html_node/emacs/Help-Echo.html
;;     "["
;;     ;; the current major mode for the buffer.
;;     '(:eval (propertize "%m" 'face nil 'help-echo buffer-file-coding-system))

;;     " "
;;     ;; buffer file encoding
;;     '(:eval (let ((sys (coding-system-plist buffer-file-coding-system)))
;;               (if (memq (plist-get sys :category)
;;                         '(coding-category-undecided coding-category-utf-8))
;;                   "UTF-8"
;;                 (upcase (symbol-name (plist-get sys :name))))))
;;     " "

;;     ;; insert vs overwrite mode, input-method in a tooltip
;;     '(:eval (propertize (if overwrite-mode "Ovr" "Ins")
;;               'face nil
;;               'help-echo (concat "Buffer is in "
;;                            (if overwrite-mode "overwrite" "insert") " mode")))

;;     ;; was this buffer modified since the last save?
;;     '(:eval (when (buffer-modified-p)
;;               (concat ","  (propertize "Mod"
;;                              'face nil
;;                              'help-echo "Buffer has been modified"))))

;;     ;; is this buffer read-only?
;;     '(:eval (when buffer-read-only
;;               (concat ","  (propertize "RO" 'face nil 'help-echo "Buffer is read-only"))))
;;     "] "

;;     ;;global-mode-string, org-timer-set-timer in org-mode need this
;;     (propertize "%M" 'face nil)

;;     " --"
;;     ;; Don't show `minor-mode'
;;     ;; minor-mode-alist  ;; list of minor modes
;;     "%-" ;; fill with '-'
;;     ))

;;----------------------------------------------------------------------------
;; 一般性编辑
;;(setq next-line-add-newlines t) ; 缓冲区结尾自动添加新行
;;自动清除行尾空格
(add-hook 'before-save-hook 'delete-trailing-whitespace)
;; whitespace 相关配置转移到 init-packages
;; 自动清除空白符号 tab spc
;; (add-hook 'before-save-hook 'whitespace-cleanup)
;; 如果是打开makefile文件，则开启indent-tabs-mode，因为whitespace-cleanup中会用到这个
;; https://www.cnblogs.com/rex-tech/p/3582646.html
;; (add-hook 'makefile-mode-hook 'indent-tabs-mode)

;;缩进始终使用空格
(setq-default indent-tabs-mode nil) ;; set indentation to always use space
(setq-default tab-width 4) ;; set default tab char's display width to 4 spaces
(setq-default fill-column 80) ; 默认列宽80
;;用一个很大的 kill ring. 防止不小心删掉重要的东西。
(setq-default kill-ring-max 500)
;; 将选中区域自动复制到系统剪贴板clipboard, 可配合Goldendict等翻译软件, 屏幕取词
(setq mouse-drag-copy-region t)
;; 鼠标 mouse 可配合前缀键, 作键绑定 , 比如 C-mouse-1
;;     mouse-1 (Left button)
;;     mouse-2 (Wheel button click / Middle button)
;;     mouse-3 (Right button)
;;     mouse-4 (Wheel up)
;;     mouse-5 (Wheel down)

;(show-paren-mode t) ;括号匹配高亮被转移到init-packages 延迟加载
;(global-hl-line-mode 1) ;当前行高亮
(fset 'yes-or-no-p 'y-or-n-p) ;设置问答提示为 y-or-n,而不是yes-or-no
;(global-font-lock-mode t)    ;语法高亮被转移到init-packages 延迟加载
(delete-selection-mode 1) ; 选中文字后输入一个字符，替换掉选中的文字
;(auto-image-file-mode t) ;打开图片显示功能被转移到init-packages 延迟加载
;; 启用narrow-to-region 与widen
;; 快捷键c-x n n 和c-x n w
(put 'narrow-to-region 'disabled nil)
;;自动加入右括号被转移到init-packages 延迟加载
;(electric-pair-mode 1)
;; (global-auto-revert-mode t);自动加载磁盘上修改过的文件被转移到init-packages 延迟加载

;; 保存访问过文件的光标位置。被转移到init-packages 延迟加载
;(save-place-mode 1)
;; (recentf-mode 1)
;; (setq recentf-max-menu-items 10)
;; (setq recentf-max-saved-items 365)
;;----------------------------------------------------------------------------
;; 自定义键绑定
;; 常用绑定和命令别名, 应在 init-evil.el 中设置
;; set-mark-command  C-;
;;(global-unset-key (kbd "C-SPC"))  ; 取消键绑定
(global-set-key (kbd "C-;") 'set-mark-command)  ; C-SPC 被输入法占用
;(global-set-key (kbd "C-M-5") 'query-replace-regexp) ; C-M-5 绑定到正则替换
;(global-set-key (kbd "C-w") 'backward-kill-word) ;shell中的快捷键同步

;;----------------------------------------------------------------------------
;; 时间设置
;;时间显示包括日期和具体时间
(setq display-time-day-and-date t)
;;启用时间显示设置，在minibuffer上面的那个杠上
(display-time)
;;;时间使用24小时制
(setq display-time-24hr-format t)
;;;时间的变化频率
;; (setq display-time-interval 60)

;; 备份文件~设置
;;----------------------------------------------------
;; 建立备份文件~的集中存储目录
;; (unless (file-exists-p "~/.em.backup") ; 判断目录是否存在
;;   (make-directory "~/.em.backup")
;;   )

;; (setq
;;    backup-by-copying t ; 自动备份
;;    backup-directory-alist
;;    '(("." . "~/.em.backup")) ; 自动备份在统一的目录
;;    delete-old-versions t ; 自动删除旧的备份文件
;;    kept-new-versions 3 ; 保留最近的3个备份文件
;; ;   kept-old-versions 1 ; 保留最早的1个备份文件
;;    version-control t) ;多次备份

;;禁止自动备份文件 ~文件
(setq make-backup-files nil)

;; #autosave file#
(setq-default auto-save-timeout 9) ; 9秒无动作,自动保存
(setq-default auto-save-interval 30) ; 30个字符间隔, 自动保存

;; 异常退出后，恢复临时文件：
;; 1. 打开要恢复的文件，如：test.txt
;; 2. [M-x]
;; 3. 输入:recover-file 回车
;; 4. 确认恢复

;; 时间戳设置
;;----------------------------------------------------------------------------
;;时间戳设置(time-stamp)，设定文档上次保存的信息
;;只要里在你得文档里有Time-stamp:的设置，就会自动保存时间戳
;;启用time-stamp
;; (setq time-stamp-active t)
;; ;;去掉time-stamp的警告？
;; (setq time-stamp-warn-inactive t)
;; ;;设置time-stamp的格式，我如下的格式所得的一个例子：<hans 05/18/2004 12:01:12>
;; (setq time-stamp-format "Last saved time: %02m/%02d/%04y %3a %02H:%02M:%02S, Editor: %:u")
;; ;;(setq time-stamp-format "%:u %02m/%02d/%04y %02H02M02S")
;; ;;将修改时间戳添加到保存文件的动作里。
;; ;;(add-hook 'write-file-hooks 'time-stamp)
;; (add-hook 'before-save-hook 'time-stamp)

;;---------------------------------------------------------------------
;; 正则表达式 regular expression
;; M-x re-build是非常好的调试Emacs正则式的工具

;;---------------------------------------------------------------------

(provide 'init-config)
